#!/usr/bin/env perl

use strict;
use warnings;

my $source_realm = "";
my $dest_realm = "";
my $threshold_difference;
my $threshold_pct;
my $threshold_demand;

$source_realm = $ARGV[0];
$dest_realm = $ARGV[1];
$threshold_difference = $ARGV[2] || 1000;
$threshold_pct = $ARGV[3] || 10;
$threshold_demand = $ARGV[4] || 0.1;

my $fh_src;
my $fh_dest;

my %dest_hash;
my $line;
my @array;
my $key;
my $name;
my $count;
my $price;

my $average_price;

open ($fh_src, "<", $source_realm) or die "Could not open file '$source_realm' $!";
open ($fh_dest, "<", $dest_realm) or die "Could not open file '$dest_realm' $!";

# skip first line
my $header = <$fh_dest>;
while ($line = <$fh_dest>) {
  $line =~ s/\s*\z//;
  @array = split /,/, $line;
  $key = $array[4];
  $dest_hash{$key} = [@array];
}
close $fh_dest;

# skip first line
$header = <$fh_src>;
while ($line = <$fh_src> ) {
 $line =~ s/\s*\z//;
 @array = split /,/, $line;
 $key = $array[4];

 $name = $array[5];
 $count = $array[7];
 $price = $array[8];

 if ($count > 0) {
     #print $key , "\n";
     if (exists ($dest_hash{$key})) {
        #print "$key EXISTS \n";
     }
     else {
        #print "$key, $name DOES NOT EXIST \n";
        next;
     }

	 my @dest = @{$dest_hash{$key}};
     #print "@dest\n";
     if (scalar(@dest) > 0) {
		 my $dest_price = $dest[8];
         #print $dest_price, "\n";

		 if (($dest_price > $price) && (($dest_price - $price) > $threshold_difference)) {
			 my $average_price = $dest[10];
			 my $demand = $dest[17];
			 my $pct = (($dest_price - $price) / $price) * 100;

			 #print $key, ":", $name, ":", $count, ":", $price, " ==> $pct $dest_price $average_price $demand \n";

			 if ($pct > 5 && $demand > 0.15) {
				 print "$key, $name, $count, $price, $dest_price, $pct, $demand  \n";
             }
		 }
		 else {
			 # print $key, ": too expensive \n"
         }
	 }
 }

}
close $fh_src;
