FROM ubuntu:latest
MAINTAINER Jon Eaves <jon@eaves.org>

#Setup basic environment
ENV DEBIAN_FRONTEND=noninteractive LANG=en_US.UTF-8 LC_ALL=C.UTF-8 LANGUAGE=en_US.UTF-8

#Update system and install packages
RUN [ "apt-get", "-q", "update" ]
RUN [ "apt-get", "-qy", "--force-yes", "upgrade" ]
RUN [ "apt-get", "-qy", "--force-yes", "dist-upgrade" ]
RUN [ "apt-get", "install", "-qy", "--force-yes", \
      "perl", \
      "build-essential", \
      "cpanminus" ]
RUN [ "apt-get", "clean" ]
RUN [ "rm", "-rf", "/var/lib/apt/lists/*", "/tmp/*", "/var/tmp/*" ]

#Copy script.pl and  make executable
COPY [ "./find_them.pl", "/app/find_them.pl" ]
RUN [ "chmod", "+x",  "/app/find_them.pl" ]

#Set entrypoint of script.pl
ENTRYPOINT [ "/app/find_them.pl" ]

