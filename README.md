# README #

### What is this repository for? ###

* cah - to Compare Auction Houses

This small Perl script allows those people transferring realms to see what is a good set of items to buy on their
source realm, and then sell again on their destination realm.

### How do I get set up? ###

* Summary of set up
	Make sure you have perl installed somewhere
	Create an account on http://www.wowuction.com/
	Use the "Data Export" feature under Resources to download CSV files for the realms

* Configuration
	perl find_them.pl source_realm_file.csv dest_realm_file.csv min_gold_difference pct_increase demand

	defaults are min_gold = 1000, pct_increase = 10, demand = 0.1
